FROM java:8-jdk
RUN apt-get update -y
RUN apt-get install -y openssh-server initscripts passwd git
RUN groupadd jenkins && useradd -d /home/jenkins -s /bin/sh -g jenkins jenkins
RUN echo "jenkins:jenkins" | chpasswd

RUN mkdir /var/run/sshd
RUN echo "session    required     pam_mkhomedir.so skel=/etc/skel/ umask=0022" >> /etc/pam.d/sshd

EXPOSE 22
CMD /usr/sbin/sshd -D
